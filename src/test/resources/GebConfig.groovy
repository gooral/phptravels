package test.resources

import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.os.ExecutableFinder


baseNavigatorWaiting = true
atCheckWaiting = true

baseUrl = 'http://www.phptravels.net/'


File findDriverExecutable() {
    def defaultExecutable = new ExecutableFinder().find("chromedriver")
    if (defaultExecutable) {
        new File(defaultExecutable)
    } else {
        new File("drivers").listFiles().findAll {
            !it.name.endsWith(".version")
        }.find {
            if (IS_OS_LINUX) {
                it.name.contains("linux")
            } else if (IS_OS_MAC) {
                it.name.contains("mac")
            } else if (IS_OS_WINDOWS) {
                it.name.contains("windows")
            }
        }
    }
}


driver = { 
	
	def pathToDriver = 'C:\\users\\mateusz.goral\\drivers\\chromedriver.exe'
	System.setProperty("webdriver.chrome.driver", pathToDriver)
	
    def driverInstance = new ChromeDriver();
    driverInstance.manage().window().maximize()
    driverInstance
}

