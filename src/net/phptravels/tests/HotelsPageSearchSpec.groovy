package net.phptravels.tests

import geb.spock.GebSpec
import net.phptravels.pages.HotelsPage
import spock.lang.Unroll

class HotelsPageSearchSpec extends GebSpec {

	@Unroll
	def "Valid test results shown for given search criteria"() {
		
		given:
		HotelsPage hotelsPage = to HotelsPage

		when:
		starForm(stars).click(HotelsPage)
		typeForm(type).click(HotelsPage)
		amenitiesForm(amenities).click(HotelsPage)
		
		searchBtn.click()

		then:
		assert $(starForm(stars)).parent().classes().contains("checked")
		assert $(typeForm(type)).parent().classes().contains("checked")
		assert $(amenitiesForm(amenities)).parent().classes().contains("checked")
		assert $(resultsTable).children().children().size() == numberOfResults
		assert $("strong.go-text-right").text() == numberOfResults.toString()
		
		for (result in resultsNames ) {
			assert $(resultsTable.find("b", text: result))			
		}
		
		where:
		stars 	| type 		| amenities 	| numberOfResults 	| resultsNames
		"5" 	| "Motel" 	| "Night Club" 	| 2 				| ["Tria Hotel Istanbul�", "Jumeirah Beach Hotel"]
	}
}
