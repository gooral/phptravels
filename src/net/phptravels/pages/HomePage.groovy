package net.phptravels.pages

import geb.Page

class HomePage extends Page{
	
	static url = "http://www.phptravels.net/";
	//static at = { title == "PHPTRAVELS | Travel Technology Partner" }
	static content = {
		hotelsLink (to: HotelsPage) { $("a", title: "Hotels") }
		//premiumLink (to: PremiumPage) { $(id: "") }
		//supportLink (to: SupportPage) { $(id: "") }
	}

}
