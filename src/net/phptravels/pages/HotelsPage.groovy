package net.phptravels.pages

import geb.Page

class HotelsPage extends Page {

	static url = "http://www.phptravels.net/hotels";
	static at = {
		waitFor(30) { title == "Hotels Listings"}
	}

	static content = {
		starForm 		{ id -> $("input[type='radio']", name: "stars", id: id).next(class: "iCheck-helper") }
		typeForm 		{ id -> $("input[type='checkbox']", name: "type[]", id: id).next(class: "iCheck-helper") }
		amenitiesForm 	{ id -> $("input[type='checkbox']", name: "amenities[]", id: id).next(class: "iCheck-helper") }
		resultsTable 	{ $("table[class='bgwhite table table-striped'") }
				
		searchBtn 		{ $("button[id=searchform") } 
		
	}
	
}
